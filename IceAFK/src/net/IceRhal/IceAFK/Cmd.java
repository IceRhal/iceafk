package net.IceRhal.IceAFK;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class Cmd implements CommandExecutor {

	FileConfiguration config = IceAFK.getPlugin().getConfig();
	IceAFK plugin = IceAFK.getPlugin();
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
		
		if(commandLabel.equalsIgnoreCase("iceafk") || commandLabel.equalsIgnoreCase("iafk")) {
			
			Player p = (Player) sender;
			String prefix = "[" + ChatColor.BLUE + "IceAFK" + ChatColor.WHITE + "] ";
			String error = prefix + ChatColor.RED + "Command error, type /iceafk help for help.";
			
			
			if(p.hasPermission("iceafk.admin")) {
				
				if(args.length == 1) {
					
					if(args[0].equalsIgnoreCase("kickplayer")) {
						
						if(config.getBoolean("kick_player")) {
							
							config.set("kick_player", false);
							plugin.saveConfig();
							p.sendMessage(prefix + ChatColor.GREEN + "Now players are not kick when AFK");
						}
						else {
							
							config.set("kick_player", true);
							plugin.saveConfig();
							p.sendMessage(prefix + ChatColor.GREEN + "Now players are kick when AFK");
						}
					}
					else if(args[0].equalsIgnoreCase("help") || args[0].equalsIgnoreCase("?")) {
						
						p.sendMessage(ChatColor.DARK_GREEN + "#### " + ChatColor.GOLD + "IceAFK - Help" 
								+ ChatColor.DARK_GREEN + " ####");
						p.sendMessage(ChatColor.GREEN + "/iceafk afktime <number> " + ChatColor.YELLOW 
								+ "define after how long of inactivity player is AFK");
						p.sendMessage(ChatColor.GREEN + "/iceafk kickplayer <true|false> " + ChatColor.YELLOW
								+ "define if player is kick when AFK");
						p.sendMessage(ChatColor.GREEN + "/iceafk kicktime <number> " + ChatColor.YELLOW 
								+ "define after player is AFK time before kick (if kick player is true)");
						p.sendMessage(ChatColor.DARK_GREEN + "#### " + ChatColor.GOLD + "Plugin by IceRhal" 
								+ ChatColor.DARK_GREEN + " ####");
					}
					else {
						
						p.sendMessage(error);
					}
				}
				else if(args.length == 2) {
					
					if(args[0].equalsIgnoreCase("kicktime")) {
						
						if(IceAFK.isInteger(args[1])) {
							
							int value = Integer.valueOf(args[1]);
							
							if(value >= 0) {
								
								config.set("kick_time", value);
								plugin.saveConfig();
								p.sendMessage(prefix + ChatColor.GREEN + "Kick time is now " + ChatColor.GOLD
										+ value + ChatColor.GREEN + " minutes");
							}
							else {
								
								p.sendMessage(prefix + ChatColor.RED + "Number must be positive");
							}
						}
						else {
							
							p.sendMessage(prefix + ChatColor.RED + "Command error, correct usage : /iceafk kicktime <number>");
						}
					}
					
					else if(args[0].equalsIgnoreCase("afktime")) {
						
						if(IceAFK.isInteger(args[1])) {
							
							int value = Integer.valueOf(args[1]);
							
							if(value > 0) {
								
								config.set("time_afk", value);
								plugin.saveConfig();
								p.sendMessage(prefix + ChatColor.GREEN + "AFK time is now " + ChatColor.GOLD
										+ value + ChatColor.GREEN + " minutes");
							}
							else {
								
								p.sendMessage(prefix + ChatColor.RED + "Number must be positive (and not 0)");
							}
						}
						else {
							
							p.sendMessage(prefix + ChatColor.RED + "Command error, correct usage : /iceafk afktime <number>");
						}
					}
					
					else if(args[0].equalsIgnoreCase("kickplayer")) {
						
						if(args[1].equalsIgnoreCase("true") || args[1].equalsIgnoreCase("t")) {
							
							config.set("kick_player", true);
							plugin.saveConfig();
							p.sendMessage(prefix + ChatColor.GREEN + "Now players are kick when AFK");
						}
						else if(args[1].equalsIgnoreCase("false") || args[1].equalsIgnoreCase("f")) {
							
							config.set("kick_player", false);
							plugin.saveConfig();
							p.sendMessage(prefix + ChatColor.GREEN + "Now players are not kick when AFK");
						}
						else {
							
							p.sendMessage(ChatColor.RED + "Command error, correct usage : /iceafk kickplayer <true|false>");							
						}
					}
					else {
						
						p.sendMessage(error);
					}
				}
				else {
					
					p.sendMessage(error);
				}
			}			
			else {
				
				p.sendMessage(prefix + ChatColor.RED + "You don't have permission (" + ChatColor.GOLD + " iceafk.admin " 
						+ ChatColor.RED + ")" );
			}
		}
		
		return true;
	}		
}
