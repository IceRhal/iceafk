package net.IceRhal.IceAFK;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.plugin.java.JavaPlugin;

public class IceAFK extends JavaPlugin {
	
	static IceAFK plugin;
	String prefix;
	HashMap<Player, Float> pitch = new HashMap<Player, Float>();
	HashMap<Player, Float> yaw = new HashMap<Player, Float>();
	HashMap<Player, Date> minAFK = new HashMap<Player, Date>();
	ArrayList<Player> playerAFK = new ArrayList<Player>();
	FileConfiguration config;
	
	public void onEnable() {
	
		plugin = this;
		
		saveDefaultConfig();
		
		config = getConfig();
		
		getCommand("iceafk").setExecutor(new Cmd());
		
		prefix = config.getString("message.prefix").replace("&", ChatColor.COLOR_CHAR + "") + " ";
		String timeAFK = config.getString("time_afk");
		String kickTime = config.getString("kick_time");
		
		if(!isInteger(timeAFK) && Integer.parseInt(timeAFK) <= 0) {
			
			config.set("time_afk", 15);
			plugin.saveConfig();
		}
		
		if(!isInteger(kickTime) && Integer.parseInt(kickTime) < 0) {
			
			config.set("kick_time", 5);
			plugin.saveConfig();
		}
		
		getServer().getScheduler().runTaskTimerAsynchronously(plugin, new Runnable() {
			
			@Override
			public void run() {
			
				Collection<? extends Player> players = Bukkit.getOnlinePlayers();
				
				for(final Player p : players) {
					
					final Location loc = p.getLocation();
					
					if(!pitch.containsKey(p) || !yaw.containsKey(p)) {
						
						pitch.put(p, loc.getPitch());
						yaw.put(p, loc.getYaw());
						continue;
					}
					
					if(!pitch.get(p).equals(loc.getPitch()) && !yaw.get(p).equals(loc.getYaw())) {
						
						if(playerAFK.contains(p)) {
							
							Bukkit.broadcastMessage(prefix
									+ config.getString("message.isntafk").replace("&", ChatColor.COLOR_CHAR + "")
											.replace("$player", p.getName()));
							
							playerAFK.remove(p);
						}
						
						minAFK.remove(p);
						pitch.put(p, loc.getPitch());
						yaw.put(p, loc.getYaw());
						continue;
					}
					
					if(!minAFK.containsKey(p)) minAFK.put(p, new Date());
					
					int timeAFK = config.getInt("time_afk");
					
					if(playerAFK.contains(p)) {
						
						if(!config.getBoolean("kick_player") || p.hasPermission("iceafk.bypass")) continue;
						
						Iterator<PermissionAttachmentInfo> it = p.getEffectivePermissions().iterator();
						
						int kickTime = config.getInt("kick_time");
						
						while(it.hasNext()) {
							
							String perm = it.next().getPermission();
							
							if(!perm.startsWith("iceafk.kickmin.")) continue;
							
							String nb = perm.substring(15);
							
							if(!isInteger(nb) && kickTime <= Integer.parseInt(nb)) continue;
							
							kickTime = Integer.parseInt(nb);
						}
						
						if(kickTime * 60000 > new Date().getTime() - minAFK.get(p).getTime()) continue;
						
						if(pitch.get(p) != loc.getPitch() && yaw.get(p) != loc.getYaw()) continue;
						
						pitch.remove(p);
						yaw.remove(p);
						
						kickAFK(p, kickTime);
						
						minAFK.remove(p);
						playerAFK.remove(p);
						
						continue;
					}
					
					if(timeAFK * 60000 < new Date().getTime() - minAFK.get(p).getTime()) continue;
					
					playerAFK.add(p);
					
					Bukkit.broadcastMessage(prefix
							+ config.getString("message.isafk").replace("&", ChatColor.COLOR_CHAR + "")
									.replace("$player", p.getName()));
				}
			}
		}, 1200L, 1200L);
	}
	
	public void kickAFK(final Player p, final int kickTime) {
	
		Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
			
			@Override
			public void run() {
			
				if(config.contains("change_kick_action")) {
					
					for(String cmd : config.getStringList("change_kick_action")) {
						
						Bukkit.dispatchCommand(
								Bukkit.getConsoleSender(),
								cmd.replace("&", ChatColor.COLOR_CHAR + "").replace("$player", p.getName())
										.replace("$afk_time", kickTime + ""));
					}
					
					return;
				}
				
				Bukkit.broadcastMessage(prefix
						+ config.getString("message.kick_atall").replace("&", ChatColor.COLOR_CHAR + "")
								.replace("$player", p.getName()).replace("$afk_time", kickTime + ""));
				
				p.kickPlayer(prefix
						+ config.getString("message.kick_atplayer").replace("&", ChatColor.COLOR_CHAR + "")
								.replace("$afk_time", kickTime + ""));
			}
		}, 1L);
	}
	
	public static IceAFK getPlugin() {
	
		return plugin;
	}
	
	public static void registerEvents(org.bukkit.plugin.Plugin plugin, Listener... listeners) {
	
		for(Listener listener : listeners)
			Bukkit.getServer().getPluginManager().registerEvents(listener, plugin);
	}
	
	public static boolean isInteger(String str) {
	
		try {
			
			Integer.parseInt(str);
			return true;
		} catch(NumberFormatException nfe) {}
		
		return false;
	}
	
}